package string;
public class StringEndsWithMethod {

	public static void main(String[] args) {
		
		String str = new String ("This is corona time.");
		boolean result;
		
		result = str.endsWith("time.");
		System.out.println(result);
		
		result = str.endsWith("corona");
		System.out.println(result);
	
	}
}
