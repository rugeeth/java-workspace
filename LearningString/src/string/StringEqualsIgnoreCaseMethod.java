package string;

public class StringEqualsIgnoreCaseMethod {

	public static void main(String[] args) {
		
		String str1 = new String ("This is beatiful World");
		String str2 = str1;
		String str3 = new String ("THIS IS BEATIFUL WORLD");
		
		boolean result;
		
		result = str1.equals(str2);
		System.out.println("Returned Value = " + result);
		
		result = str3.equalsIgnoreCase(str3);
		System.out.println("Returned Value = " + result);
		
	}
}
