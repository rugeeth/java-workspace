package string;

public class StringEqualsMethod {

	public static void main(String[] args) {
		
		String str1 = new String ("Thalapathy is a best actor");
		String str2 = str1;
		String str3 = new String ("Thalapathy is a best actor");
		boolean result;
		
		result = str2.equals(str1);
		System.out.println(result);
		
		result = str3.equals(str3);
		System.out.println(result);
	}
}
