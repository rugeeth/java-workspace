package string;

public class StringIndexOf {

	public static void main(String[] args) {
		
		String str = new String("Java is best Programe");
		String str2 = new String("programe");
		
		System.out.print("Found index : ");
		System.out.println(str.indexOf('e', 5)); // String indexOf(int ch, int fromIndex)
		
		System.out.print("Found index : ");
		System.out.println(str.indexOf('r')); // Java � String indexOf() Method
		
		System.out.println("Found index : " + str.indexOf(str2)); //String indexOf(String str) Method
		
		System.out.print("Found index : ");
		System.out.println(str.indexOf(str2, 10)); // String indexOf(String str, int fromIndex)
	}
}
