package string;

public class StringLastIndexOfMethod {

	public static void main(String[] args) {
		
		String str = new String ("I LOVE YOU 3000");
		String str1 = new String ("YOU");
		
		System.out.println("last index of : " + str.lastIndexOf('O')); // lastIndexOf(int ch);
		
		System.out.println("last index of : " + str.lastIndexOf('O', 4)); // lastIndexOf(ch, fromIndex);
		
		System.out.println("last index of : " + str.lastIndexOf(str1)); // lastIndexOf(String str);
		
		System.out.println("last index of : " + str.lastIndexOf(str1, 8)); // lastIndexOf(str1, fromIndex);
	}
}
