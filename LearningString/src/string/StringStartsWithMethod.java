package string;

public class StringStartsWithMethod {

	public static void main(String[] args) {
		
		String str = new String ("The earth is now in danger");
		
		System.out.println("Return Value : " + str.startsWith("The"));
		System.out.println("Return Value : " + str.startsWith("danger"));
		System.out.println();
		
		System.out.print("Return Value : ");
		System.out.println(str.startsWith("earth", 4));
		
	}
}
