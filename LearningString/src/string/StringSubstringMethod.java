package string;

public class StringSubstringMethod {

	public static void main(String[] args) {
		
		String str = "The earth is now in danger";
		
		System.out.println(str.substring(4));
		System.out.println();
		System.out.println(str.substring(4, 9)); //substring(beginIndex, endIndex)
	}
}
