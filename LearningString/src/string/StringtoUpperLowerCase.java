package string;

public class StringtoUpperLowerCase {

	public static void main(String[] args) {
		 
		String str = "The earth is now in danger";
		String str2 = "THE EARTH IS NOW IN DANGER";
		
		System.out.println(str.toUpperCase());
		System.out.println();
		System.out.println(str2.toLowerCase());
	}
}
